# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from datetime import datetime, timedelta, time
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, In, Get, Or, Bool, Not
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, Button, StateReport, StateTransition
from trytond.report import Report

__all__ = ['CustomerService', 'RegionDepartment',
           'ReceiverService', 'DepartmentEmail', 'HealthProvider',]

STATES = {
    'readonly': (Eval('state') != 'draft'),
}

# class UserFiduprevisora(ModelSQL, ModelView):
#     'User Fiduprevisora'
#     __name__ = 'crm.user_fiduprevisora'
#     _rec_name = 'id_number'
#     name = fields.Char('Name', required=True)
#     id_number = fields.Char('ID Number', required=True)
#     type_document = fields.Char('Type Document',)
#     city_attention = fields.Many2One('party.city_code', 'City Attention')
#     state = fields.Selection([
#                 ('', ''),
#                 ('retirado', 'Retirado'),
#                 ('activo', 'Activo'),
#                 ('proteccion_laboral', 'Protección Laboral'),
#             ], 'State')
#     state_string = state.translated('state')
#
#     @classmethod
#     def __setup__(cls):
#         super(UserFiduprevisora, cls).__setup__()
#         cls._error_messages.update({
#             'error_import': '%s',
#         })
#
#     @classmethod
#     def search_rec_name(cls, name, clause):
#         if clause[1].startswith('!') or clause[1].startswith('not '):
#             bool_op = 'AND'
#         else:
#             bool_op = 'OR'
#         return [bool_op,
#                 ('id_number',) + tuple(clause[1:]),
#                 ('name',) + tuple(clause[1:]),
#                 ]
#
#     @classmethod
#     def import_data(cls, fields_names, data):
#         pool = Pool()
#         City = pool.get('party.city_code')
#         # data_ = []
#         number_ids = []
#         cont = 0
#         for row in data[1:]:
#             name = row[0] + ' ' + row[1] + ' ' + row[2] + ' ' + row[3]
#             if row[5] in number_ids:
#                 continue
#             number_ids.append(row[5])
#             users = cls.search([
#                 ('id_number', '=', row[5]),
#             ])
#             state_ = ''
#             if row[8] == 'Activo':
#                 state_ = 'activo'
#             elif row[8] == 'Retirado':
#                 state_ = 'retirado'
#             elif row[8] == 'Protección Laboral':
#                 state_ = 'proteccion_laboral'
#
#             if users:
#                 if state_:
#                     cls.write([users], {'state': state_})
#             else:
#                 try:
#                     cities = City.search([
#                     ('code', '=', row[6]),
#                     ])
#                     create_user_ = {
#                         'name': name,
#                         'id_number': row[5],
#                         'type_document': row[4],
#                         'city_attention': cities[0].id if cities else None,
#                         'state': state_,
#                     }
#                     cls.create([create_user_])
#                     cont += 1
#                 except:
#                     cls.raise_user_error('error_import', 'Error en la linea ' + name + ' [' + row[5] + ']')
#             # data_.append(row)
#         return cont
#         # return super(UserFiduprevisora, cls).import_data(fields_names, data_)


# class FiduprevisoraDepartment(ModelSQL, ModelView):
#     'Fiduprevisora Department'
#     __name__ = 'crm.fiduprevisora_department'
#     _rec_name = 'name'
#     name = fields.Char('Name', required=True)
#     region = fields.Many2One('crm.region_fiduprevisora', 'Region',
#             required=True, select=True)
#     emails = fields.One2Many('crm.fiduprevisora_department.email',
#            'department', 'Emails')
#
#
# class FiduprevisoraCity(ModelSQL, ModelView):
#     'Fiduprevisora City'
#     __name__ = 'crm.fiduprevisora_city'
#     _rec_name = 'name'
#     name = fields.Char('Name', required=True)
#     code = fields.Char('Code')
#     department = fields.Many2One('crm.fiduprevisora_department', 'Department',
#             required=True, select=True)


class ReceiverService(ModelSQL, ModelView):
    'Receicer Service'
    __name__ = 'crm.receiver_service'
    _rec_name = 'name'
    name = fields.Char('Name', required=True)

    def get_rec_name(self, name):
        self._rec_name = self.name
        return (self._rec_name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('name',) + tuple(clause[1:]),
                ]


class HealthProvider(ModelSQL, ModelView):
    'Health Provider'
    __name__ = 'crm.health_provider'
    _rec_name = 'party'
    name = fields.Function(fields.Char('Name'), 'get_rec_name')
    party = fields.Many2One('party.party', 'Party')
    city = fields.Many2One('party.city_code', 'City')

    def get_rec_name(self, name):
        self._rec_name = self.party.name
        return (self._rec_name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('party.name',) + tuple(clause[1:]),
                ]


class CustomerService(metaclass=PoolMeta):
    __name__ = 'crm.customer_service'
    receiver = fields.Many2One('crm.receiver_service', 'Receiver', states={
        'required': (Eval('state') == 'open'),
    })
    # user = fields.Many2One('crm.user_fiduprevisora', 'Documento Usuario', required=True)
    region = fields.Many2One('crm.region_fiduprevisora', 'Region')
    department_region = fields.Many2One('party.department_code', 'Department', domain=[
        ('region', '=', Eval('region'))
        ])
    city_region = fields.Many2One('party.city_code', 'City', depends=['party'])
    other_city = fields.Char('Other City', states={
        'required': (Not(Bool(Eval('city_region')))),
        'invisible': (Bool(Eval('city_region'))),
    })
    health_provider = fields.Many2One('crm.health_provider', 'Health Provider', domain=[
            ('city', '=', Eval('city_region'))
        ])
    attach_customer = fields.Char('Attach Customer', readonly=True)
    id_document = fields.Char('Document')
    email = fields.Char('Email')
    sended_mail = fields.Boolean('Sended Email')
    attach_response = fields.Binary('Attach Response')
    # other_receivers = fields.One2Many('crm.customer_receiver',
    #        'customer_service', 'Other Receivers', states={
    #             'invisible': (Eval('state') == 'draft'),
    #         })

    @classmethod
    def __setup__(cls):
        super(CustomerService, cls).__setup__()
        cls.party.required = True
        new_sel = [
                ('', ''),
                ('sede', 'Sede'),
                ('web', 'Web'),
                ('supersalud', 'Supersalud'),
        ]
        if new_sel not in cls.media.selection:
            cls.media.selection = new_sel

        cls._buttons.update({
            'send_email': {
                'invisible': Or(
                    Eval('state') != 'open',
                    Bool(Eval('sended_mail')),
                )},
            })
        cls._error_messages.update({
            'message': '%s',
        })

    @classmethod
    @ModelView.button
    def send_email(cls, records):
        for service in records:
            service.send_email_customer()
            # if service.receiver and service.receiver.email:
            #     service.send_emails_receivers(service.receiver.email)
            # for rec in service.other_receivers:
            #     if rec.receiver.email:
            #         service.send_emails_receivers(rec.receiver.email)

    @staticmethod
    def default_region():
        Configuration = Pool().get('crm.configuration')
        config = Configuration(1)
        if config and config.region_defect:
            return config.region_defect.id

    @fields.depends('party', 'address', 'phone')
    def on_change_party(self):
        super(CustomerService, self).on_change_party()
        if self.party:
            self.email = self.party.email
            self.phone = self.party.phone
            self.address = self.party.street
            # self.id_document = self.party.id_number

    @fields.depends('party', 'city_region', 'department_region')
    def on_change_party(self):
        if self.party:
            self.city_region = self.party.city_attention.id
            if self.city_region and self.city_region.department:
                self.department_region = self.city_region.department.id

    @fields.depends('effective_date', 'response')
    def on_change_with_effective_date(self):
        if self.response:
            return datetime.now()

    @classmethod
    def create(cls, services):
        services = super(CustomerService, cls).create(services)
        for service in services:
            if not service.department_region and service.city_region:
                cls.write([service], {'department_region': service.city_region.department})
        return services


    @classmethod
    def open(cls, services):
        res = cls.validate_service(services)
        if res:
            super(CustomerService, cls).open(services)
            for service in services:
                if service.email or party.email:
                    service.send_email_notification(service.email or party.email)
                    service.send_emails_department()
                    print('-----------------emails enviado------------------')

                # if service.id_document:
                #     Party = Pool().get('party.party')
                #     party = Party.search([
                #         ('id_number', '=', service.id_document),
                #     ])
                #     if not party:
                #         party, = Party.create([{
                #             'name': service.customer,
                #             'id_number': service.id_document,
                #             'contact_mechanisms': [
                #             ('create', [
                #             {'type': 'email', 'value': service.email or ' '},
                #             {'type': 'mobile', 'value': service.phone or ' '},
                #             ])
                #             ],
                #         }])
                #         print('---------------------Tercero creado---------------------')
                    # cls.write([service],{'party': party.id})


    @classmethod
    def validate_service(cls, services):
        # Party = Pool().get('party.party')
        for service in services:
            # users = Party.search([
            #     ('id_number', '=', service.id_document)
            # ])
            # if not users:
            #     service.get_message('El usuario: ' + service.customer+ ' no esta registrado en la base de datos')
            if service.party and service.party.affiliation_state == 'retirado':
                service.get_message('El usuario: ' + service.party.name+ ' Se encuentra en estado retirado')
            date_ = service.cs_date - timedelta(days=30)

            services_ = cls.search([
                ('id', '!=', service.id),
                ('cs_date', '>=', date_),
                ('case', '=', service.case.id),
                ('party.id_number', '=', service.party.id_number),
            ])
            if services_:
                service.get_message('Ya existe un PQR para el usuario: ' + service.party.name \
                                    + ' con el caso: "' + service.case.name + '" en los últimos 30 días')
            return True

    def get_extension(self, string_):
        ext_ = ''
        if string_.find('PDF') > 0:
            ext_ = 'PDF'
        elif string_.find('PNG') > 0:
            ext_ = 'PNG'
        elif string_.find('JPEG') > 0:
            ext_ = 'JPEG'
        elif string_.find('JPG') > 0:
            ext_ = 'JPG'
        elif string_.find('DOC') > 0:
            ext_ = 'DOC'
        elif string_.find('ODS') > 0:
            ext_ = 'ODS'
        elif string_.find('ODT') > 0:
            ext_ = 'ODT'
        return ext_

    def send_email_customer(self, file_name='respuesta'):
        # print(self.attach_response)
        q = str(self.attach_response)[1:10]
        ext_ = self.get_extension(q)
        pool = Pool()
        config = pool.get('crm.configuration')(1)
        Template = pool.get('email.template')
        template = config.response_mail_template
        if template:
            attach_dict = {}
            attach = False
            template.subject = template.subject + self.company.party.name
            if self.attach_response:
                attach = True
                attach_dict = {
                    'attachment': self.attach_response,
                    'file_name': file_name,
                    'extension': ext_,
                }
                response = self._send_mails(template, self,  self.party.email or self.email, attach, [attach_dict])
            else:
                response = self._send_mails(template, self,  self.party.email or self.email, attach, [])
            if response.status_code == 202:
                self.write([self], {'sended_mail': True,})
            else:
                self.get_message('Error de Envío.')
        else:
            self.get_message('No se ha definido una plantilla para el envío del correo.')

    def send_email_notification(self, email):
        pool = Pool()
        Template = pool.get('email.template')
        config = pool.get('crm.configuration')(1)
        template = config.notification_mail_template
        if template:
            attach_dict = {}
            template.subject = template.subject + self.company.party.name
            response = Template.send(template, self, email,
                attach=False, attachments=[])
            if response.status_code != 202:
                self.write({self}, {'notes': 'Fallo envio al correo: ' + email})

    def send_emails_department(self):
        if self.department_region:
            for email in self.department_region.emails:
                self.send_email_notification(email.email)


    def send_emails_receivers(self, email):
        if email:
            pool = Pool()
            config = pool.get('crm.configuration')(1)
            template = config.response_mail_template
            if template:
                template.subject = template.subject + self.company.party.name
                response = self._send_mails(template, self,  email, False, [])
                if response.status_code != 202:
                    self.write({self}, {'notes': 'Fallo envio al correo: ' + email})

    def _send_mails(self, template, crm,  email, attach, attachments):
        Template = Pool().get('email.template')
        response = Template.send(template, crm, email,
            attach=attach, attachments=attachments)
        return response

    def get_message(self, message):
        self.raise_user_error('message', message)

    @classmethod
    def dash_on_change_user(cls, args, ctx):
        if not args.get('party'):
            return {}

        Party = Pool().get('party.party')
        parties = Party.browse([args['party']['id']])

        if not parties:
            return {}

        res = {
            'customer': parties[0].name,
            'media': 'web',
            'city_region': {
                'id': parties[0].city_attention.id,
                'name': parties[0].city_attention.name,
            },
            'state': 'draft',
        }
        if parties[0].affiliation_state == 'retirado':
            res['state'] = 'cancelled'
        return res


class FiduprevisoraReportStart(ModelView):
    'Fiduprevisora Report Start'
    __name__ = 'crm_fiduprevisora.fiduprevisora_report.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    party = fields.Many2One('party.party', 'Party')
    region = fields.Many2One('crm.region_fiduprevisora', 'Region')
    receiver_service = fields.Many2One('crm.receiver_service', 'Receiver')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end():
        Date = Pool().get('ir.date')
        return Date.today()


class FiduprevisoraReportWizard(Wizard):
    'Fiduprevisora Report Wizard'
    __name__ = 'crm_fiduprevisora.fiduprevisora_report.wizard'
    start = StateView('crm_fiduprevisora.fiduprevisora_report.start',
        'crm_fiduprevisora.fiduprevisora_report_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('crm_fiduprevisora.fiduprevisora_report')

    def do_print_(self, action):

        party_id = self.start.party.id if self.start.party else None
        region_id = self.start.region.id if self.start.region else None
        receiver_service_id = self.start.receiver_service.id if self.start.receiver_service else None

        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'party': party_id,
            'region': region_id,
            'receiver_service': receiver_service_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class FiduprevisoraReport(Report):
    'Fiduprevisora Report'
    __name__ = 'crm_fiduprevisora.fiduprevisora_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(FiduprevisoraReport, cls).get_context(records, data)
        pool = Pool()
        Company = pool.get('company.company')
        Service = pool.get('crm.customer_service')
        company = Company(data['company'])
        start_date = data['start_date']
        end_date = data['end_date']
        party = data['party']
        region = data['region']
        receiver_service = data['receiver_service']
        records = {}

        dom_service = [
                ('company', '=', data['company']),
                ('state', '!=', 'cancelled'),
        ]

        if start_date:
            start_date = datetime.combine(start_date, time(0,0))
        if end_date:
            end_date = datetime.combine(end_date, time(23,59))
            dom_service.append(
            ('cs_date', '<=', end_date),
            )
        if party:
            dom_service.append(
            ('party', '=', party),
            )
        if region:
            dom_service.append(
            ('region', '=', region),
            )
        if receiver_service:
            dom_service.append(
            ('receiver', '=', receiver_service),
            )
        services = Service.search(dom_service)

        report_context['records'] = services
        report_context['start_date'] = start_date
        report_context['end_date'] = end_date
        report_context['company'] = company.party.name
        return report_context


# class CustomerReceiver(ModelSQL, ModelView):
#     'Customer Receiver'
#     __name__ = 'crm.customer_receiver'
#     customer_service = fields.Many2One('crm.customer_service', 'Customer Service', required=True)
#     receiver = fields.Many2One('crm.receiver_service', 'Receiver')
#     media = fields.Selection([
#             ('', ''),
#             ('sede', 'Sede'),
#             ('web', 'Web'),
#             ('supersalud', 'Supersalud'),
#         ], 'Media')
