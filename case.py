# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta

__all__ = ['Case']


class Case(metaclass=PoolMeta):
    __name__ = "crm.case"
    macroprocess = fields.Char('Macroprocess')
    process_service = fields.Char('Process Service')
    motives_health = fields.Char('Motives General Health')
    type_request = fields.Char('Type Request')
    priority = fields.Char('Priority')
    time_max = fields.Numeric('Time Max Response (days)', digits=(2, 0))
    quality_attribute = fields.Char('Quality Attribute')
    required_attach = fields.Boolean('Required Attach')


    def get_rec_name(self, name):
        return self.name
