#This file is part product_barcode module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
import configuration
import customer_service
import case
import party


def register():
    Pool.register(
        configuration.Configuration,
        case.Case,
        party.RegionFiduprevisora,
        party.DepartmentCode,
        party.Party,
        # customer_service.UserFiduprevisora,
        # customer_service.FiduprevisoraDepartment,
        # customer_service.FiduprevisoraCity,
        # customer_service.FiduprevisoraRegionDepartment,
        customer_service.ReceiverService,
        customer_service.CustomerService,
        customer_service.FiduprevisoraReportStart,
        customer_service.HealthProvider,
        party.DepartmentEmail,
        # customer_service.CustomerReceiver,
        module='crm_fiduprevisora', type_='model')
    Pool.register(
        customer_service.FiduprevisoraReportWizard,
        module='crm_fiduprevisora', type_='wizard')
    Pool.register(
        customer_service.FiduprevisoraReport,
        module='crm_fiduprevisora', type_='report')
