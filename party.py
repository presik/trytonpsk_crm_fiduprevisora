# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If, In, Get, Or, Bool, Not
from trytond.transaction import Transaction

__all__ = ['RegionFiduprevisora', 'DepartmentCode', 'Party']


class RegionFiduprevisora(ModelSQL, ModelView):
    'Region Fiduprevisora'
    __name__ = 'crm.region_fiduprevisora'
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    departments = fields.Function(fields.One2Many('crm.fiduprevisora_department', None,
        'Departments'), 'get_departments')

    def get_departments(self, name):
        Departments = Pool().get('crm.fiduprevisora_department')
        departments = Departments.search([
            ('region', '=', self.id)
        ])
        if departments:
            return [d.id for d in departments]
        else:
            return []


class DepartmentCode(metaclass=PoolMeta):
    __name__ = "party.department_code"
    region = fields.Many2One('crm.region_fiduprevisora', 'Region')
    emails = fields.One2Many('crm.fiduprevisora_department.email',
               'department', 'Emails')


class DepartmentEmail(ModelSQL, ModelView):
    'Department - Emails'
    __name__ = 'crm.fiduprevisora_department.email'
    department = fields.Many2One('party.department_code', 'Department', required=True)
    email = fields.Char('Email')


class Party(metaclass=PoolMeta):
    __name__ = "party.party"
    is_affiliate_user = fields.Boolean('Is Affiliate User')
    city_attention = fields.Many2One('party.city_code', 'City Attention', states={
        'invisible': (Not(Bool(Eval('is_affiliate_user')))),
    })
    affiliation_state = fields.Selection([
                ('', ''),
                ('retirado', 'Retirado'),
                ('activo', 'Activo'),
                ('proteccion_laboral', 'Protección Laboral'),
            ],'Affiliation State', states={
                'invisible': (Not(Bool(Eval('is_affiliate_user')))),
            })

    @classmethod
    def create(cls, vlist):
        return super(Party, cls).create(vlist)


    @classmethod
    def import_data(cls, fields_names, data):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        City = pool.get('party.city_code')
        Sequence = pool.get('ir.sequence.strict')
        # data_ = []
        number_ids = []
        cont = 0
        for row in data[1:]:
            print(cont)
            code = cls._new_code()
            name = row[0] + ' ' + row[1] + ' ' + row[2] + ' ' + row[3]
            if row[5] in number_ids:
                continue
            number_ids.append(row[5])
            users = cls.search([
                ('id_number', '=', row[5]),
            ])
            state_ = ''
            if row[8] == 'Activo':
                state_ = 'activo'
            elif row[8] == 'Retirado':
                state_ = 'retirado'
            elif row[8] == 'Protección Laboral' or row[4].count('Protec') > 0:
                state_ = 'proteccion_laboral'

            if users:
                if state_:
                    cls.write([users[0]], {'affiliation_state': state_})
            else:
                try:
                    cities = City.search([
                        ('code', '=', row[6]),
                    ])
                    id_number = row[5]
                    if row[4].count('Ciud') > 0:
                        type_document = 13
                    elif row[4].count('Pasa') > 0:
                        type_document = 41
                    elif row[4].count('Tarj') > 0:
                        type_document = 12
                    elif row[4].count('Regi') > 0:
                        type_document = 11
                    else:
                        type_document = 0
                    if cities:
                        city_attention = cities[0].id
                        cursor.execute("INSERT INTO party_party (active, is_affiliate_user, code, \
                                       name, id_number, type_document, city_attention, affiliation_state) \
                                       VALUES ('true', 'true', '%s', '%s', '%s', '%s', %s, '%s')" % (
                                           code, name, id_number, type_document, city_attention, state_
                                           )
                                       )
                    else:
                        cursor.execute("INSERT INTO party_party (active, is_affiliate_user, code, \
                        name, id_number, type_document, affiliation_state) \
                        VALUES ('true', 'true', '%s', '%s', '%s', '%s', '%s')" % (
                        code, name, id_number, type_document, state_
                        )
                        )
                    cont += 1
                except:
                    cls.raise_user_error('error_import', 'Error en la linea ' + name )
        return cont
